HSSIP Analysis
==============

Analysis code example prepared for the students taking part in HSSIP 2017.
Some simple histogram filling routines using AnalysisBase-21.2, reading
MC16a primary xAODs.

Getting the code
----------------

To download the code go to a (preferably) empty directory, and clone
the Git repository with:

```bash
git clone https://gitlab.cern.ch/akraszna/HSSIPAnalysis.git
```

Building the code
-----------------

To build the code, you have to set up the AnalysisBase-21.2
release. On lxplus you could use one of the recent nightly
build. Like:

```bash
asetup AnalysisBase,21.2,r25
```

But with a local build of the analysis release, just source the
`setup.sh` file of the release. Like:

```bash
source ~/ATLAS/sw/AnalysisBase/21.2.0/InstallArea/x86_64-mac1012-clang81-opt/setup.sh
```

With the release set up, you can build this project using CMake, using
an out of source build.

```bash
mkdir build
cd build/
cmake ../HSSIPAnalysis/
make
```

Running the code
----------------

To set up your locally built code, you need to source the `setup.sh`
file generated in your build directory. So, from scratch you would set
up your shell to run the repository's applications, like:

```bash
source ~/ATLAS/sw/AnalysisBase/21.2.0/InstallArea/x86_64-mac1012-clang81-opt/setup.sh
source ./build/x86_64-mac1012-clang81-opt/setup.sh
```
