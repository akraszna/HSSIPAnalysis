
// System include(s):
#include <cstring>

// ROOT include(s):
#include <TDirectory.h>

// Local include(s):
#include "CPPAnalysis/HistogramMgr.h"

// Declare the class to ROOT:
ClassImp( hssip::HistogramMgr )

namespace hssip {

   HistogramMgr::HistogramMgr()
      : ::TObject(), m_hists(), m_histMap() {

   }

   void HistogramMgr::writeTo( ::TDirectory& dir ) const {

      // Change to the target directory:
      dir.cd();

      // Iterate over all managed histograms:
      for( auto& hist : m_hists ) {
         // Write this histogram:
         hist->Write();
         // Tell the user what happened:
         Info( "writeTo", "Histogram \"%s\" written", hist->GetName() );
      }
   }

   void HistogramMgr::readFrom( ::TDirectory& /*dir*/ ) {

      // To be implemented...
   }

   void HistogramMgr::add( std::unique_ptr< ::TH1 > hist ) {

      // Check if we have a histogram with this name already:
      auto map_itr = m_histMap.find( hist->GetName() );
      if( map_itr != m_histMap.end() ) {
         // Remove that histogram from the vector:
         auto vec_itr = m_hists.begin();
         auto vec_end = m_hists.end();
         for( ; vec_itr != vec_end; ++vec_itr ) {
            if( ::strcmp( ( *vec_itr )->GetName(), hist->GetName() ) ) {
               continue;
            }
            m_hists.erase( vec_itr );
            break;
         }
      }

      // Add the histogram to the vector holding all histograms:
      m_hists.push_back( std::move( hist ) );
      auto& histRef = m_hists.back();

      // Disconnect the histogram from any ROOT directory:
      histRef->SetDirectory( nullptr );

      // And add / update a pointer in the histogram map:
      m_histMap[ histRef->GetName() ] = histRef.get();

      // Tell the user what happened:
      Info( "add", "Histogram \"%s\" added to memory", histRef->GetName() );
   }

   const ::TH1& HistogramMgr::hist( const std::string& name ) const {

      // Look for the histogram:
      auto itr = m_histMap.find( name );

      // In case we didn't succeed:
      if( itr == m_histMap.end() ) {
         Error( "hist", "Couldn't find histogram with name \"%s\"",
                name.c_str() );
         throw std::runtime_error( "Couldn't find histogram with name \"" +
                                   name + "\"" );
      }

      // If we did:
      return *( itr->second );
   }

   ::TH1& HistogramMgr::hist( const std::string& name ) {

      // Look for the histogram:
      auto itr = m_histMap.find( name );

      // In case we didn't succeed:
      if( itr == m_histMap.end() ) {
         Error( "hist", "Couldn't find histogram with name \"%s\"",
                name.c_str() );
         throw std::runtime_error( "Couldn't find histogram with name \"" +
                                   name + "\"" );
      }

      // If we did:
      return *( itr->second );
   }

   HistogramMgr::hist_const_iterator HistogramMgr::begin() const {

      return m_hists.begin();
   }

   HistogramMgr::hist_const_iterator HistogramMgr::end() const {

      return m_hists.end();
   }

   HistogramMgr::hist_iterator HistogramMgr::begin() {

      return m_hists.begin();
   }

   HistogramMgr::hist_iterator HistogramMgr::end() {

      return m_hists.end();
   }
   
} // namespace hssip
