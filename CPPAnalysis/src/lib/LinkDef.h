// Dear emacs, this is -*- c++ -*-
#ifndef CPPANALYSIS_LINKDEF_H
#define CPPANALYSIS_LINKDEF_H

// Some common definitions:
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

// Declare the class(es) to generate dictionaries for:
#pragma link C++ namespace hssip;
#pragma link C++ class hssip::HistogramMgr+;
#pragma link C++ class hssip::Analyzer+;

#endif // CPPANALYSIS_LINKDEF_H
