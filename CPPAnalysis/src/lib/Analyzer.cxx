
// System include(s):
#include <vector>
#include <algorithm>
#include <iostream>
#include <cmath>

// ROOT include(s):
#include <TH1.h>
#include <TH2.h>
#include <TLorentzVector.h>

// EDM include(s):
#include "xAODCore/tools/PrintHelpers.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

// Local include(s):
#include "CPPAnalysis/Analyzer.h"
#include "CPPAnalysis/HistogramMgr.h"

// Declare the class to ROOT:
ClassImp( hssip::Analyzer )

/// Helper macro checking StatusCode return values
#define CHECK( LOC, EXP )                                       \
   do {                                                         \
      const auto sc = EXP;                                      \
      if( isFailure( sc ) ) {                                   \
         Error( #LOC, "Failed to execute\"%s\"", #EXP );        \
         throw std::runtime_error( "Failed to execute " #EXP ); \
      }                                                         \
   } while( 0 )

namespace {

   /// Function checking if a StatusCode is in failure mode
   bool isFailure( const StatusCode& sc ) {
      return sc.isFailure();
   }

   /// Function checking if a CorrectionCode is in failure mode
   bool isFailure( const CP::CorrectionCode& cc ) {
      return ( cc == CP::CorrectionCode::Error );
   }

} // private namespace

namespace hssip {

   /// Multiplier to convert MeV to GeV
   static const double MeVtoGeV = 0.001;

   Analyzer::Analyzer()
      : ::TObject(), m_elCalib( "ElectronCalibrator" ),
        m_elSelect( "ElectronSelector" ) {

   }

   void Analyzer::initialize( HistogramMgr& hMgr ) {

      // Configure and initialise the CP tool(s):
      /*
      CHECK( "initialize", m_elCalib.setProperty( "ESModel",
                                                  "es2016data_mc15c_summer" ) );
      CHECK( "initialize",
             m_elCalib.setProperty( "randomRunNumber",
                                    EgammaCalibPeriodRunNumbersExample::run_2016 ) );
      CHECK( "initialize", m_elCalib.initialize() );

      CHECK( "initialize", m_elSelect.setProperty( "WorkingPoint",
                                                   "MediumLHElectron" ) );
      CHECK( "initialize", m_elSelect.initialize() );
      */

      // Make sure that histograms collect statistical uncertainties:
      TH1::SetDefaultSumw2();

      // Create the histograms:
      hMgr.add( std::make_unique< TH1D >( "el_pt_uncalib",
                                          "Electron p_{T} uncalibrated "
                                          "distribution;"
                                          "p_{T} [GeV];Entries",
                                          100, 0.0, 100.0 ) );
      hMgr.add( std::make_unique< TH2D >( "el_etaphi",
                                          "Electron #eta-#phi distribution;"
                                          "#eta;#phi;Entries",
                                          50, -3.0, 3.0,
                                          50, -M_PI, M_PI ) );

      hMgr.add( std::make_unique< TH1D >( "z_mass_uncalib",
                                          "Z candidate uncalibrated mass "
                                          "distribution;"
                                          "m [GeV/c^{2}];Entries",
                                          100, 20.0, 120.0 ) );
      hMgr.add( std::make_unique< TH2D >( "z_etaphi",
                                          "Z candidate #eta-#phi distribution;"
                                          "#eta;#phi;Entries",
                                          50, -3.0, 3.0,
                                          50, -M_PI, M_PI ) );

      /*
      hMgr.add( std::make_unique< TH1D >( "el_pt_calib",
                                          "Electron p_{T} calibrated "
                                          "distribution;"
                                          "p_{T} [GeV];Entries",
                                          100, 0.0, 100.0 ) );
      hMgr.add( std::make_unique< TH1D >( "z_mass_calib",
                                          "Z candidate calibrated mass "
                                          "distribution;"
                                          "m [GeV/c^{2}];Entries",
                                          40, 20.0, 120.0 ) );
       */
   }

   void Analyzer::finalize( HistogramMgr& /*hMgr*/ ) {

   }

   void Analyzer::analyze( const xAOD::EventInfo& ei,
                           const xAOD::VertexContainer& vertices,
                           const xAOD::ElectronContainer& electrons,
                           HistogramMgr& hMgr ) {

      // Fill some simple monitoring histograms for the electrons:
      for( const xAOD::Electron* el : electrons ) {
         hMgr.hist( "el_pt_uncalib" ).Fill( el->pt() * MeVtoGeV );
         dynamic_cast< ::TH2& >( hMgr.hist( "el_etaphi" ) ).Fill( el->eta(),
                                                                  el->phi() );
      }

      // Run the simplest analysis:
      analyzeSimple( electrons, hMgr );

      // Run the "calibrated" analysis:
      //analyzeCalib( ei, vertices, electrons, hMgr );
   }

   void Analyzer::analyze( const xAOD::EventInfo& /*ei*/,
                           const xAOD::VertexContainer& /*vertices*/,
                           const xAOD::MuonContainer& /*muons*/,
                           HistogramMgr& /*hMgr*/ ) {

   }

   void Analyzer::analyzeSimple( const xAOD::ElectronContainer& electrons,
                                 HistogramMgr& hMgr ) {

      // Select electrons passing some simple quality requirements:
      std::vector< const xAOD::Electron* > goodElectrons;
      for( const xAOD::Electron* el : electrons ) {
         if( ( el->pt() > 20000.0 ) && ( el->eta() < 2.4 ) ) {
            goodElectrons.push_back( el );
         }
      }

      // Stop here if we don't have at least two good electrons:
      if( goodElectrons.size() < 2 ) {
         return;
      }

      // Sort the selected electrons according to their transverse momentum:
      std::sort( goodElectrons.begin(), goodElectrons.end(), Analyzer::ptSort );

      // Take the two highest pT electrons:
      const xAOD::Electron* el1 = goodElectrons[ 0 ];
      const xAOD::Electron* el2 = goodElectrons[ 1 ];

      // Print all of the properties of the first two good electrons:
      static bool propertiesPrinted = false;
      if( ! propertiesPrinted ) {
         std::cout << "Properties of the selected electrons:" << std::endl;
         std::cout << *el1 << std::endl << *el2 << std::endl;
         propertiesPrinted = true;
      }

      // Assuming that the two electrons are decay products of a single
      // particle, reconstruct the 4-momentum of that particle:
      const ::TLorentzVector zCand = el1->p4() + el2->p4();

      // Save the properties of this Z candidate:
      hMgr.hist( "z_mass_uncalib" ).Fill( zCand.M() * MeVtoGeV );
      dynamic_cast< ::TH2& >( hMgr.hist( "z_etaphi" ) ).Fill( zCand.Eta(),
                                                              zCand.Phi() );
   }

   void Analyzer::analyzeCalib( const xAOD::EventInfo& ei,
                                const xAOD::VertexContainer& vertices,
                                const xAOD::ElectronContainer& electrons,
                                HistogramMgr& hMgr ) {

      // First off, select the primary vertex of the event:
      const xAOD::Vertex* priVtx = nullptr;
      for( const xAOD::Vertex* vtx : vertices ) {
         if( vtx->vertexType() == xAOD::VxType::PriVtx ) {
            priVtx = vtx;
            break;
         }
      }

      // If no primary vertex was found, bail:
      if( ! priVtx ) {
         return;
      }

      // Make a shallow copy of the electron container:
      auto shallowCopy = xAOD::shallowCopyContainer( electrons );
      // Make sure that we take ownership of the objects created in memory:
      std::unique_ptr< xAOD::ElectronContainer >
         electronsCopy( shallowCopy.first );
      std::unique_ptr< xAOD::ShallowAuxContainer >
         electronsShallowAux( shallowCopy.second );

      // Calibrate the shallow copies:
      for( xAOD::Electron* el : *electronsCopy ) {
         CHECK( "analyzeCalib", m_elCalib.applyCorrection( *el ) );
         hMgr.hist( "el_pt_calib" ).Fill( el->pt() * MeVtoGeV );
      }

      // Select the "good" electrons:
      std::vector< const xAOD::Electron* > goodElectrons;
      for( const xAOD::Electron* el : *electronsCopy ) {
         // Apply some basic kinematic cuts first:
         if( ! ( ( el->pt() > 20000.0 ) && ( el->eta() < 2.4 ) &&
                 ( std::abs( el->caloCluster()->etaBE( 2 ) ) < 2.47 ) ) ) {
            continue;
         }
         // Apply a transverse impact parameter significance cut:
         const double d0sig =
            xAOD::TrackingHelpers::d0significance( el->trackParticle(),
                                                   ei.beamPosSigmaX(),
                                                   ei.beamPosSigmaY(),
                                                   ei.beamPosSigmaXY() );
         if( d0sig > 5.0 ) {
            continue;
         }
         // Apply a longitudinal impact parameter cut:
         const double deltaZ0 = std::abs( el->trackParticle()->z0() +
                                          el->trackParticle()->vz() -
                                          priVtx->z() );
         const double z0Sig = deltaZ0 * std::sin( el->trackParticle()->theta() );
         if( z0Sig > 0.5 ) {
            continue;
         }
         // Apply a likelihood selection:
         if( ! m_elSelect.accept( el ) ) {
            continue;
         }

         // The electron is selected:
         goodElectrons.push_back( el );
      }

      // Stop here if we don't have at least two good electrons:
      if( goodElectrons.size() < 2 ) {
         return;
      }

      // Sort the selected electrons according to their transverse momentum:
      std::sort( goodElectrons.begin(), goodElectrons.end(), Analyzer::ptSort );

      // Take the two highest pT electrons:
      const xAOD::Electron* el1 = goodElectrons[ 0 ];
      const xAOD::Electron* el2 = goodElectrons[ 1 ];

      // Assuming that the two electrons are decay products of a single
      // particle, reconstruct the 4-momentum of that particle:
      const ::TLorentzVector zCand = el1->p4() + el2->p4();

      // Save the mass of this Z candidate:
      hMgr.hist( "z_mass_calib" ).Fill( zCand.M() * MeVtoGeV );
   }

   bool Analyzer::ptSort( const xAOD::IParticle* p1,
                          const xAOD::IParticle* p2 ) {

      return p1->pt() > p2->pt();
   }

} // namespace hssip
