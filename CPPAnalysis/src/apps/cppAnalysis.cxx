/// @file cppAnalysis.cxx
/// @brief Main application file for the C++ analysis
///
/// This example runs the full-blown analysis example written in for the
/// HSSIP students in C++.

// System include(s):
#include <iostream>
#include <vector>
#include <string>
#include <memory>

// Boost include(s):
#include <boost/program_options.hpp>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>

// AnalysisBase tool include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

// AnalysisBase EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/VertexContainer.h"

// Local include(s):
#include "CPPAnalysis/HistogramMgr.h"
#include "CPPAnalysis/Analyzer.h"

// A helpful alias for the boost::program_options namespace:
namespace po = boost::program_options;

int main( int argc, char* argv[] ) {

   // The name of the application:
   static const char* APP_NAME = "cppAnalysis";

   // Declare what program options are expected by the application:
   po::options_description desc( "cppAnalysis command line arguments" );
   desc.add_options()
      ( "help,h", "Provide usage help" )
      ( "input,i",
        po::value< std::vector< std::string > >()->multitoken(),
        "Input xAOD file(s)" )
      ( "output,o", po::value< std::string >(), "Output histogram file" );

   // Interpret the command line arguments:
   po::variables_map vm;
   try {
      po::store( po::parse_command_line( argc, argv, desc ), vm );
      po::notify( vm );
   } catch( const std::exception& ex ) {
      std::cout << "Command line parsing failed with: " << ex.what()
                << std::endl;
      return 1;
   }

   // If the user asked for help:
   if( vm.count( "help" ) ) {
      std::cout << desc << std::endl;
      return 0;
   }

   // Make sure that at least one input file was provided:
   if( ( ! vm.count( "input" ) ) ||
       ( ! vm[ "input" ].as< std::vector< std::string > >().size() ) ) {
      std::cout << "You need to provide at least one input file!" << std::endl
                << std::endl;
      std::cout << desc << std::endl;
      return 1;
   }

   // Get the input file names:
   const std::vector< std::string > inputFiles =
      vm[ "input" ].as< std::vector< std::string > >();

   // Get the output file name:
   const std::string outputFile = ( vm.count( "output" ) ?
                                    vm[ "output" ].as< std::string >() :
                                    "output.root" );

   // If we got this far, it's time to initialise the application for xAOD
   // reading:
   RETURN_CHECK( APP_NAME, xAOD::Init() );

   // Tell the user what options we got:
   Info( APP_NAME, "Will be analysing file(s):" );
   for( const std::string& fileName : inputFiles ) {
      Info( APP_NAME, "  - %s", fileName.c_str() );
   }
   Info( APP_NAME, "Will be writing to file: %s", outputFile.c_str() );

   // Create the event reading object:
   xAOD::TEvent event;
   // ...and a transient store object:
   xAOD::TStore store;

   // Create the histogram manager:
   hssip::HistogramMgr hMgr;

   // Create the analyser object, and initialise it:
   hssip::Analyzer analyzer;
   analyzer.initialize( hMgr );

   // Global processed events counter:
   ::Long64_t processedEvents = 0;

   // Loop over the input files:
   for( const std::string& fileName : inputFiles ) {

      // Open the file:
      std::unique_ptr< ::TFile > ifile( ::TFile::Open( fileName.c_str(),
                                                       "READ" ) );
      // Make sure that it succeeded:
      if( ( ! ifile.get() ) || ifile->IsZombie() ) {
         ::Error( APP_NAME, "Failed to open file: %s", fileName.c_str() );
         return 1;
      }
      ::Info( APP_NAME, "Opened file: %s", fileName.c_str() );

      // Connect the TEvent object to it:
      RETURN_CHECK( APP_NAME, event.readFrom( ifile.get() ) );

      // Loop over the events of the file:
      const ::Long64_t entries = event.getEntries();
      for( ::Long64_t entry = 0; entry < entries; ++entry ) {

         // Load the event:
         if( event.getEntry( entry ) < 0 ) {
            ::Error( APP_NAME, "Failed to load entry %lli from file: %s",
                     entry, fileName.c_str() );
            return 1;
         }

         // Retrieve the event info object:
         const xAOD::EventInfo* ei = 0;
         RETURN_CHECK( APP_NAME, event.retrieve( ei, "EventInfo" ) );

         // Print a nice status message for every 500 events processed:
         if( ! ( processedEvents % 500 ) ) {
            ::Info( APP_NAME, "===>>> Processing Run #%d, Event #%lld "
                    "(#%lld events processed) <<<===",
                    ei->runNumber(), ei->eventNumber(), processedEvents );
         }
         ++processedEvents;

         // Retrieve the reconstructed vertices from the event:
         const xAOD::VertexContainer* vertices = 0;
         RETURN_CHECK( APP_NAME, event.retrieve( vertices,
                                                 "PrimaryVertices" ) );

         // Retrieve the reconstructed leptons from the event:
         const xAOD::ElectronContainer* electrons = 0;
         RETURN_CHECK( APP_NAME, event.retrieve( electrons, "Electrons" ) );
         const xAOD::MuonContainer* muons = 0;
         RETURN_CHECK( APP_NAME, event.retrieve( muons, "Muons" ) );

         // Give the containers to the analyser:
         analyzer.analyze( *ei, *vertices, *electrons, hMgr );
         analyzer.analyze( *ei, *vertices, *muons, hMgr );
      }
   }

   // Finalise the analysis:
   analyzer.finalize( hMgr );

   // Open the output file:
   std::unique_ptr< ::TFile > ofile( ::TFile::Open( outputFile.c_str(),
                                                    "RECREATE" ) );
   if( ( ! ofile.get() ) || ofile->IsZombie() ) {
      ::Error( APP_NAME, "Couldn't open output file: %s", outputFile.c_str() );
      return 1;
   }

   // Write all the histograms to the output:
   hMgr.writeTo( *ofile );
   ::Info( APP_NAME, "Histograms written to: %s", outputFile.c_str() );

   // Return gracefully:
   return 0;
}
