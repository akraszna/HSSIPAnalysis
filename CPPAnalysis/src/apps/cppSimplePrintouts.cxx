/// @file cppSimplePrintouts.cxx
/// @brief Application providing some simple printouts during xAOD reading
///
/// This example stands on its own, only picking up libraries from inside the
/// analysis release to run. It reads in an xAOD file given to it on the command
/// line, sets up its reading, and then prints some simple information out of
/// it.

// System include(s):
#include <memory>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>

// AnalysisBase tool include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

// AnalysisBase EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"

int main( int argc, char* argv[] ) {

   // The name of the application:
   static const char* APP_NAME = "cppSimplePrintouts";

   // The application expects a single command line argument. The name of the
   // file that should be read.
   if( ( argc != 2 ) || ( ! ::strcmp( argv[ 1 ], "-h" ) ) ) {
      ::Info( APP_NAME, "Usage: %s <xAOD name>", APP_NAME );
      return 1;
   }

   // Initialise the application for xAOD reading:
   RETURN_CHECK( APP_NAME, xAOD::Init() );

   // Open the file specified:
   std::unique_ptr< ::TFile > ifile( ::TFile::Open( argv[ 1 ], "READ" ) );
   // Check that we succeeded:
   if( ( ! ifile.get() ) || ifile->IsZombie() ) {
      ::Error( APP_NAME, "Couldn't open file: %s", argv[ 1 ] );
      return 1;
   }
   // Tell the user what happened:
   ::Info( APP_NAME, "Opened file: %s", argv[ 1 ] );

   // Set up the object used in reading the file:
   xAOD::TEvent event;
   RETURN_CHECK( APP_NAME, event.readFrom( ifile.get() ) );

   // Loop over all events of the file:
   const ::Long64_t entries = event.getEntries();
   for( ::Long64_t entry = 0; entry < entries; ++entry ) {

      // Load the appropriate event:
      if( event.getEntry( entry ) < 0 ) {
         ::Error( APP_NAME, "Failed to load entry %lld", entry );
         return 1;
      }

      // Load the EventInfo from the event:
      const xAOD::EventInfo* ei = 0;
      RETURN_CHECK( APP_NAME, event.retrieve( ei, "EventInfo" ) );

      // Now prints some information about the event:
      ::Info( APP_NAME, "Now processing run #%d, event #%lld, lbn #%d "
              "(%lld entries processed so far)",
              ei->runNumber(), ei->eventNumber(), ei->lumiBlock(), entry );

      // Load the electron and muon containers:
      const xAOD::ElectronContainer* electrons = 0;
      RETURN_CHECK( APP_NAME, event.retrieve( electrons, "Electrons" ) );
      const xAOD::MuonContainer* muons = 0;
      RETURN_CHECK( APP_NAME, event.retrieve( muons, "Muons" ) );

      // Print some info about them. Note that the return type of "size()" is
      // platform specific. So it's easiest to just forcefully cast it to
      // something (int) to avoid problems on all platforms.
      ::Info( APP_NAME, "Leptons in the event: electrons = %i, muons = %i",
              static_cast< int >( electrons->size() ),
              static_cast< int >( muons->size() ) );
   }

   // Return gracefully:
   return 0;
}
