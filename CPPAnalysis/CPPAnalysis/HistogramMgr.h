// Dear emacs, this is -*- c++ -*-
#ifndef CPPANALYSIS_HISTOGRAMMGR_H
#define CPPANALYSIS_HISTOGRAMMGR_H

// System include(s):
#include <vector>
#include <map>
#include <string>
#include <memory>

// ROOT include(s):
#include <TObject.h>
#include <TH1.h>

// Forward declaration(s):
class TDirectory;

/// Namespace for all of the HSSIP code
namespace hssip {

   /// Helper class managing the histograms used by the analysis
   ///
   /// This is just to demonstrate how to organise a simple histogram filling
   /// analysis code such that it could be reasonably extened into a much larger
   /// analysis if necessary.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class HistogramMgr : public ::TObject {

   public:
      /// Default constructor
      HistogramMgr();

      /// Non-const iterator type for the managed histograms
      typedef std::vector< std::unique_ptr< ::TH1 > >::iterator hist_iterator;
      /// Const iterator type for the managed histograms
      typedef std::vector< std::unique_ptr< ::TH1 > >::const_iterator
      hist_const_iterator;

      /// @name Input/Output functions
      /// @{

      /// Write the managed histograms into a ROOT directory
      void writeTo( ::TDirectory& dir ) const;
      /// Read in the histograms from a directory/file
      void readFrom( ::TDirectory& dir );

      /// @}

      /// @name Histogram accessors
      /// @{

      /// Function adding a new histogram to the manager
      void add( std::unique_ptr< ::TH1 > hist );

      /// Access one of the managed histograms by name (const)
      const ::TH1& hist( const std::string& name ) const;
      /// Access one of the managed histograms by name (non-const)
      ::TH1& hist( const std::string& name );

      /// Get the beginning of the histogram range (const)
      hist_const_iterator begin() const;
      /// Get the end of the histogram range (const)
      hist_const_iterator end() const;

      /// Get the beginning of the histogram range (non-const)
      hist_iterator begin();
      /// Get the end of the histogram range (non-const)
      hist_iterator end();

      /// @}

   private:
      /// @name Data members
      /// @{

      /// Vector holding the histograms in memory
      std::vector< std::unique_ptr< ::TH1 > > m_hists;
      /// Convenience map for quickly finding the histograms
      std::map< std::string, ::TH1* > m_histMap;

      /// @}

      // Declare the class to ROOT:
      ClassDef( hssip::HistogramMgr, 0 )

   }; // class HistogramMgr

} // namespace hssip

#endif // CPPANALYSIS_HISTOGRAMMGR_H
