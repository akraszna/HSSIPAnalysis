// Dear emacs, this is -*- c++ -*-
#ifndef CPPANALYSIS_ANALYZER_H
#define CPPANALYSIS_ANALYZER_H

// ROOT include(s):
#include <TObject.h>

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/VertexContainer.h"

// Tool include(s):
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"

// Forward declaration(s):
namespace xAOD {
   class IParticle;
}

namespace hssip {

   // Forward declaration(s):
   class HistogramMgr;

   /// Class used for analysing ATLAS reconstructed events
   ///
   /// This example demonstrates how to select electrons and muons in
   /// a simple way to try to reconstruct Z bosons out of them.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class Analyzer : public ::TObject {

   public:
      /// Default constructor
      Analyzer();

      /// @name Setup / tear down functions
      /// @{

      /// Function initialising the analysis object
      void initialize( HistogramMgr& hMgr );

      /// Function finalising the operations of the analysis object
      void finalize( HistogramMgr& hMgr );

      /// @}

      /// @name Analysis functions
      /// @{

      /// Function analysing an event's reconstructed electrons
      void analyze( const xAOD::EventInfo& ei,
                    const xAOD::VertexContainer& vertices,
                    const xAOD::ElectronContainer& electrons,
                    HistogramMgr& hMgr );

      /// Function analysing an event's reconstructed muons
      void analyze( const xAOD::EventInfo& ei,
                    const xAOD::VertexContainer& vertices,
                    const xAOD::MuonContainer& muons,
                    HistogramMgr& hMgr );

      /// @}

   private:
      /// @name Analysis functions with different complexity
      /// @{

      /// Function analysing electrons in the simplest way imaginable
      void analyzeSimple( const xAOD::ElectronContainer& electrons,
                          HistogramMgr& hMgr );

      /// Function calibrating and analysing electrons in a slightly fancier way
      void analyzeCalib( const xAOD::EventInfo& ei,
                         const xAOD::VertexContainer& vertices,
                         const xAOD::ElectronContainer& electrons,
                         HistogramMgr& hMgr );

      /// @}

      /// Helper function ordering xAOD::IParticle objects by their pt
      static bool ptSort( const xAOD::IParticle* p1,
                          const xAOD::IParticle* p2 );

      /// @name Combined Performance tool(s):
      /// @{

      /// Tool calibrating the four momentum of electrons
      CP::EgammaCalibrationAndSmearingTool m_elCalib;

      /// Tool selecting the "good" electrons
      AsgElectronLikelihoodTool m_elSelect;

      /// @}

      // Declare the class to ROOT:
      ClassDef( hssip::Analyzer, 0 )

   }; // class Analyzer

} // namespace hssip

#endif // CPPANALYSIS_ANALYZER_H
