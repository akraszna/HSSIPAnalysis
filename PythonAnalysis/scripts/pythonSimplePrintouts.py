#!/usr/bin/env python
#
# Python script providing some simple printouts during xAOD reading
#
# This example just demonstrates how to read information from an xAOD file
# from Python, using the facilities provided by the ATLAS analysis release.

# The script needs a file name as an argument:
import sys
if ( len( sys.argv ) != 2 ) or ( sys.argv[ 1 ] == "-h" ):
    print( "Usage: %s <xAOD name>" % sys.argv[ 0 ] );
    sys.exit( 1 )
    pass

# We need ROOT:
import ROOT

# Set up the environment for xAOD reading:
ROOT.xAOD.Init().ignore()

# Open the file specified:
ifile = ROOT.TFile.Open( sys.argv[ 1 ], "READ" )
if ( not ifile ) or ifile.IsZombie():
    print( "Couldn't open file: %s" % sys.argv[ 1 ] )
    sys.exit( 1 )
    pass
# Tell the user what happened:
print( "Opened file: %s" % sys.argv[ 1 ] )

# Set up a "transient tree" from the file:
tree = ROOT.xAOD.MakeTransientTree( ifile )

# Loop over this transient tree:
for entry in xrange( tree.GetEntries() ):

    # Load the event:
    if tree.GetEntry( entry ) < 0:
        print( "Failed to load entry %i" % entry )
        sys.exit( 1 )
        pass

    # Print some information about the event:
    print( "Now processing run #%i, event #%i, lbn #%i "
           "(%i entries processed so far)" % \
               ( tree.EventInfo.runNumber(),
                 tree.EventInfo.eventNumber(),
                 tree.EventInfo.lumiBlock(), entry ) )

    # Print the number of electrons and muons in the event:
    print( "Leptons in the event: electrons = %i, muons = %i" % \
               ( tree.Electrons.size(), tree.Muons.size() ) )

    pass

# Clean up:
ROOT.xAOD.ClearTransientTrees()
