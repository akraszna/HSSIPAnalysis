Python Analysis Code
====================

This package collects example code written in Python. It mimicks the
functionality of the C++ examples, just written in a different
language.
